package com._98point6.droptoken.service;

import com._98point6.droptoken.model.*;

import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class to handle game related requests.
 */
public class DropTokenServiceImpl implements DropTokenService {

    Map<String, GameInfo> gameRecords = new HashMap<String, GameInfo>();
    private static final int GRID_SIZE = 4;

    /**
     * checks for In_Progress game state and adds them to list and returns all Active Games.
     *
     * @return It will return all Active games
     * response example -  "games": [ "a5fbd9ff-098c-43ac-a605-63c2c9c149dd", "8f017ccb-f6d6-4fe3-8b2e-8816ad23e4c8" ]
     */
    public GetGamesResponse getActiveGames() {
        List<String> activeGameIds = gameRecords.entrySet().stream().filter(e ->
                GameState.IN_PROGRESS.equals(e.getValue().getState()))
                .map(Map.Entry::getKey).collect(Collectors.toList());

        return new GetGamesResponse.Builder().games(activeGameIds).build();
    }

    /**
     * Creates a new game with randonUUID as gameID, players and game state is set as IN_PROGRESS
     *
     * @param request consists of players array, number of columns and rows
     * @return It returns gameId of newly created game
     * response example - "gameId": "8f017ccb-f6d6-4fe3-8b2e-8816ad23e4c8"
     */
    public CreateGameResponse createNewGame(CreateGameRequest request) {
        String gameId = UUID.randomUUID().toString();

        GameInfo gameInfo = new GameInfo(request.getPlayers());
        gameInfo.setState(GameState.IN_PROGRESS);
        gameRecords.put(gameId, gameInfo);

        return new CreateGameResponse.Builder()
                .gameId(gameId).build();
    }

    /**
     * gets state, winner and Players list
     *
     * @param gameId
     * @return players list, winner name, state of the game
     * sample response - { "players": ["sai","teja"], "winner": "teja",  "state": "DONE" }
     */
    @Override
    public GameStatusResponse getGameStatus(String gameId) {
        GameInfo gameInfo = gameRecords.get(gameId);
        if (gameInfo == null || gameInfo.getMovesPerformed() == null
                || gameInfo.getMovesPerformed().isEmpty()) return null;

        return new GameStatusResponse.Builder().state(gameInfo.getState()).
                winner(gameInfo.getWinner()).players(gameInfo.getPlayers()).build();
    }

    /**
     * Validates if it is players turn, valid move, performs move and set opponent as next turn
     *
     * @param gameId
     * @param playerId
     * @param column
     * @return it will return gameId and moves number.
     * sample response - "/a5fbd9ff-098c-43ac-a605-63c2c9c149dd/moves/1"
     */
    @Override
    public Response postMove(String gameId, String playerId, int column) {
        GameInfo gameInfo = gameRecords.get(gameId);
        if (gameInfo == null || !gameInfo.getPlayers().contains(playerId))
            return notFoundResponse("game/player is not found");

        int playerNumber = gameInfo.getPlayerToNumber().get(playerId);
        if (gameInfo.getPlayerTurn() == 0)
            gameInfo.setPlayerTurn(getOpponent(playerNumber));
        else if (playerNumber != gameInfo.getPlayerTurn()) return Response.status(Response.Status.CONFLICT)
                .entity("Its not " + playerId + " turn").build();

        int[][] grid = gameInfo.getGrid();
        if (!checkIfMoveValidAndPost(gameInfo, grid, column, playerId, playerNumber))
            return badRequestResponse("Illegal/Invalid move on column:" + column);

        gameInfo.setPlayerTurn(getOpponent(playerNumber)); //set opponent as next turn

        findAndState(gameInfo); // calculate state of game after posting move

        return Response.ok(new PostMoveResponse.Builder()
                .moveLink("{" + gameId + "}" + "/moves/" + (gameInfo.getMovesPerformed().size() - 1))
                .build()).build();
    }

    /**
     * Adds moves into list and return list
     *
     * @param gameId
     * @param start
     * @param until
     * @return List of type, player, column of all moves
     * sample response - [{"type": "MOVE", "player": "sai","column": 3 },
     * {"type": "MOVE","player": "teja","column": 3},
     * {"type": "MOVE","player": "sai", "column": 3} ]
     */
    @Override
    public Response getMoves(String gameId, Integer start, Integer until) {
        GameInfo gameInfo = gameRecords.get(gameId);
        if (gameInfo == null || gameInfo.getMovesPerformed().isEmpty())
            return notFoundResponse("games/moves not found");
        List<Move> moves = gameInfo.getMovesPerformed();
        int movesSize = moves.size();
        if (until == null) until = movesSize - 1;
        if (start < 0 || until < 0 || start >= movesSize || until >= movesSize)
            return badRequestResponse("invalid start/until");

        List<Move> movesReturned = new ArrayList<>();
        for (int i = start; i <= until; i++)
            movesReturned.add(moves.get(i));

        return Response.ok(movesReturned).build();
    }

    /**
     * check if moveid is valid or not and returns move
     *
     * @param gameId
     * @param moveId
     * @return type, player and column number on which the move is done
     * sample response - { "type": "MOVE", "player": "teja", "column: 3 }
     */
    @Override
    public Response getMove(String gameId, Integer moveId) {
        GameInfo gameInfo = gameRecords.get(gameId);
        if (gameInfo == null || gameInfo.getMovesPerformed().isEmpty())
            return notFoundResponse("games/moves not found");

        if (moveId < 0 || moveId >= gameInfo.getMovesPerformed().size())
            return badRequestResponse("invalid start/until");

        return Response.ok(gameInfo.getMovesPerformed().get(moveId)).build();
    }

    /**
     * sets game state done, sets opponent as winner and add moveType as QUIT
     *
     * @param gameId
     * @param playerId
     * @return response code of success or failure
     * sample response - HttpStatus code - 202 on success
     */
    @Override
    public Response playerQuit(String gameId, String playerId) {
        GameInfo gameInfo = gameRecords.get(gameId);
        if (gameInfo == null || !gameInfo.getPlayers().contains(playerId))
            return notFoundResponse("game not found or player is not part of it");
        if (GameState.DONE.equals(gameInfo.getState()))
            return Response.status(Response.Status.GONE).build();

        gameInfo.setState(GameState.DONE);
        gameInfo.setWinner(gameInfo.getNumberToPlayer().get(
                getOpponent(gameInfo.getPlayerToNumber().get(playerId))));

        List<Move> moves = gameInfo.getMovesPerformed();
        moves.add(new Move.Builder().player(playerId).type(MoveType.QUIT).build());
        gameInfo.setMovesPerformed(moves);  // add quit move to the game

        return Response.accepted().build();
    }

    /**
     * sets gameInfo with winner and state
     *
     * @param gameInfo
     */
    private void findAndState(GameInfo gameInfo) {
        int[][] grid = gameInfo.getGrid();

        if (checkIfPlayerWins(grid, 1)) {
            gameInfo.setWinner(gameInfo.getNumberToPlayer().get(1));
            gameInfo.setState(GameState.DONE);
        }
        if (checkIfPlayerWins(grid, 2)) {
            gameInfo.setWinner(gameInfo.getNumberToPlayer().get(2));
            gameInfo.setState(GameState.DONE);
        }
        //check for Draw
        int tokensOccupied = 0;
        for (int i = 0; i < GRID_SIZE; i++)
            for (int j = 0; j < GRID_SIZE; j++)
                if (grid[i][j] != 0) tokensOccupied++;
        if (tokensOccupied == 16)
            gameInfo.setState(GameState.DONE); // set Game state to done if its draw without setting winnner.
    }

    /**
     * validates if any row/column/diagonal is filled with player tokens and returns true/false
     *
     * @param grid
     * @param playerNumber
     * @return true in case of row/column/diagonal is filled with input playerNumber
     */
    private boolean checkIfPlayerWins(int[][] grid, int playerNumber) {
        //check if any row is filled with player tokens
        for (int j = 0; j < GRID_SIZE; j++) {
            int playerTokensFilled = 0;
            for (int i = 0; i < GRID_SIZE; i++) {
                if (grid[i][j] == playerNumber) playerTokensFilled++;
            }
            if (playerTokensFilled == GRID_SIZE) return true;
        }
        //check if any column is filled with player tokens
        for (int i = 0; i < GRID_SIZE; i++) {
            int playerTokensFilled = 0;
            for (int j = 0; j < GRID_SIZE; j++) {
                if (grid[i][j] == playerNumber) playerTokensFilled++;
            }
            if (playerTokensFilled == GRID_SIZE) return true;
        }
        //check if any diagonal is filled
        int playerTokensFilled = 0;
        for (int i = 0; i < GRID_SIZE; i++) if (grid[i][i] == playerNumber) playerTokensFilled++;
        if (playerTokensFilled == GRID_SIZE) return true;

        playerTokensFilled = 0;
        for (int i = 0; i < GRID_SIZE; i++) if (grid[i][(GRID_SIZE - 1) - i] == playerNumber) playerTokensFilled++;

        return playerTokensFilled == GRID_SIZE;
    }

    /**
     * returns opponent player number
     *
     * @param playerNumber
     * @return 2 in case of input 1
     */
    private int getOpponent(int playerNumber) {
        return playerNumber == 1 ? 2 : 1;
    }

    /**
     * validates gameState,column has empty rows or not, perform move and add move to list
     *
     * @param gameInfo
     * @param grid
     * @param col
     * @param playerId
     * @param playerNumber
     * @return true in case of gameState not equal to DONE and column is not full
     */
    private boolean checkIfMoveValidAndPost(GameInfo gameInfo, int[][] grid, int col,
                                            String playerId, int playerNumber) {

        if (gameInfo.getState() == GameState.DONE) return false; // game is already done

        int rowAvailable = 0;
        while (rowAvailable < GRID_SIZE && grid[rowAvailable][col] != 0) rowAvailable++;

        if (rowAvailable > (GRID_SIZE - 1)) return false; // column is full so move is not valid

        grid[rowAvailable][col] = playerNumber; // post move
        gameInfo.setGrid(grid);

        List<Move> moves = gameInfo.getMovesPerformed();// add move to list
        moves.add(new Move.Builder().type(MoveType.MOVE).column(col).player(playerId).build());
        gameInfo.setMovesPerformed(moves);

        return true;
    }

    private Response badRequestResponse(String message) {
        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    }

    private Response notFoundResponse(String message) {
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
    }
}